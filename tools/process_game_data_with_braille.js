#!/usr/bin/env node
import fs from 'fs-extra';
import { argv } from 'process';

const [_node, _thisFile, dataPath, resultPath] = argv;

const data = fs.readJSONSync(dataPath);
const result = [];

for (const [char, braille] of data) {
	let processedBraille = '';
	for (const b of braille) {
		if (b === '.') {
			processedBraille += 'a, '
		} else if (b === '1') {
			processedBraille += 'jedna, '
		} else {
			processedBraille += `${b}, `;
		}
	}

	result.push([char, processedBraille]);
}

fs.writeJSONSync(resultPath, result, { spaces: 4 });
