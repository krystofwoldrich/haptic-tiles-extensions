#!/usr/bin/env node
import fs from 'fs-extra';
import { argv } from 'process';

const [_node, _thisFile, logPath, dataSetPath] = argv;

const {keyDownHistory, moves} = fs.readJSONSync(logPath);
const dataSet = fs.readJSONSync(dataSetPath)

let matches = 0;
let mismatchesBeforeFirst = 0;
let isFirstMatchFound = false;

moves.forEach(move => {
	const matchPair = dataSet.find(pair => {
		const pairFirst = pair[0].key
			?? pair[0].value
			?? pair[0];
		const pairSecond = pair[1].key
			?? pair[1].value
			?? pair[1];

		return false
			|| pairFirst === move[0]
			|| pairFirst === move[1]
			|| pairSecond === move[0]
			|| pairSecond === move[1];
	});

	const pairFirst = matchPair[0].key
		?? matchPair[0].value
		?? matchPair[0];
	const pairSecond = matchPair[1].key
		?? matchPair[1].value
		?? matchPair[1];

	if (matchPair
		&& (pairFirst == move[0]
			|| pairFirst == move[1])
		&& (pairSecond == move[0]
			|| pairSecond == move[1])) {
		matches++;
		isFirstMatchFound = true;
		console.log(move)
	}

	if (!isFirstMatchFound) {
		mismatchesBeforeFirst++;
	}
});

console.log(`keyDownHistory: ${keyDownHistory.length}`);
console.log(`moves: ${moves.length}`);
console.log(`matches: ${matches}`);
console.log(`mismatchesBeforeFirst: ${mismatchesBeforeFirst}`);
