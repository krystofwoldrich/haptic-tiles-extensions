# Haptic Tiles Extensions

This project is extending hardware platform Haptic Tiles to give it extra functionality for further use in education and entertainment of visually impaired individuals.

## How to run Talking Sign System

```bash
#Development
DEBUG="HapticTilesExtensions:*" ./src/index.js tss

DEBUG="HapticTilesExtensions:*" ./src/index.js tss --only-examples

DEBUG="HapticTilesExtensions:*" ./src/index.js tss -m keyboard --only-examples

#Production
./src/index.js tss --only-examples

./src/index.js tss
```

## How to run Character Education

```bash
DEBUG="HapticTilesExtensions:*" ./src/index.js char-education -d static/czech_braille.json
```

## How to run Memorize Game
```bash
#Development
DEBUG="HapticTilesExtensions:*" ./src/index.js memorize-game -d static/animals/animals_czech.json --mute -m keyboard

#Production
./src/index.js memorize-game -d static/animals/animals_czech.json

DEBUG="HapticTilesExtensions:*" ./src/index.js memorize-game -d static/animals/animals_czech.json

DEBUG="HapticTilesExtensions:*" ./src/index.js memorize-game -d static/czech_english_body.json

DEBUG="HapticTilesExtensions:*" ./src/index.js memorize-game -d static/czech_famous/czech_famous.json
```

## How to run Braille Numbering Education
```bash
#Development
DEBUG="HapticTilesExtensions:*" ./src/index.js braille-numbering

#Production
./src/index.js braille-numbering
```

## How to use braille game process tool
```bash
./tools/process_game_data_with_braille.js ./static/czech_braille_game.json ./result.json
```
