import pDebounce from 'p-debounce';
import { AudioPlayerFactory } from '../Audio/AudioPlayerFactory.mjs';
import BrailleModuleFactory from '../BrailleModule/BrailleModuleFactory.mjs'
import CharacterLearning from './CharacterLearning.mjs'

export default class EducationFactory {

	static async createCharacterLearning({ module, mute, dataPath, language }) {
		const brailleModule = await BrailleModuleFactory.createBrailleModule(module);
		const player = AudioPlayerFactory.createAudioPlayer(mute, language);

		const characterLearning = new CharacterLearning(
			brailleModule,
			(...args) => player.play(...args),
			dataPath,
		);

		await characterLearning.start();
		return characterLearning;
	}

	static async startBrailleNumbering({ module, mute, language }) {
		const brailleModule = await BrailleModuleFactory.createBrailleModule(module);
		const player = AudioPlayerFactory.createAudioPlayer(mute, language);


		const onBrailleKeyDown = async (data) => {
			const braille = data.braille;
			if (braille) {
				console.log('Said: ', braille);
				await player.play(braille);
			}
		}

		brailleModule.on(
			'keydown',
			pDebounce(onBrailleKeyDown, 50, { before: true }),
		);
	}

}
