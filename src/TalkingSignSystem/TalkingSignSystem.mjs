import Debug from 'debug';
import debounce from '../Utils/debounce.mjs';

const debug = Debug('HapticTilesExtensions:TalkingSignSystem');

export default class TalkingSignSystem {

	history = [];

	constructor(
		remote,
		play,
		onlyExamples,
	) {
		this.remote = remote;
		this.play = play;
		this.onlyExamples = onlyExamples;
	}

	async start() {
		this.remote.on(
			'keydown',
			debounce(this._onRemoteKeydown.bind(this)),
		)
	}

	async _onRemoteKeydown(key) {
		debug('pressed remote', key);
		const prevKey = this.history[this.history.length - 1];
		const secondToLastKey = this.history[this.history.length - 2];

		if (this.onlyExamples) {
			await this._playExample(key);
			return;
		}

		this.history.push(key);
		if (secondToLastKey === key && prevKey === key) {
			await this._playExample(key);
		} else if (prevKey === key) {
			await this._playMeaning(key);
		} else {
			await this.play(key);
		}
	}

	async _playExample(key) {
		switch (key) {
			case '1':
				await this.play('Pošta praha 06. Hybernská 15 Praha 1')
				break;
			case '2':
				await this.play('Po jednom schodu jsou dveře. V levo po šesti metrech jsou v levo další dveře. Otevíraání v pravo k sobě. Za nimi po 2 metrech je přepážka číslo 1, k přepážce přistupte po výzvě.')
				break;
			case '3':
				await this.play('Linka 135. Směr Chodov.')
				break;
			case '4':
				await this.play('Otevření dveří aktivováno.')
				break;
			case '5':
				await this.play('Bohužel, tady nemám ukázku.')
				break;
			case '6':
				await this.play('Osobní vlak číslo 46 56 do stanice Tišín. Pravidelný odjezd 14:46. Kolej 1')
				break;

			default:
				break;
		}
	}

	async _playMeaning(key) {
		switch (key) {
			case '1':
				await this.play('vyvolání odezvy akustického majáčku pro orientaci nevidomého')
				break;
			case '2':
				await this.play('vyvolání doplňkové informace akustického majáčku')
				break;
			case '3':
				await this.play('vyvolání hlasové informace o číslu linky a směru jízdy dopravního prostředku')
				break;
			case '4':
				await this.play('potvrzení nástupu nevidomého do dopravního prostředku a případné otevření dveří, nebo vysunutí plošiny u nových typů dopravních prostředků')
				break;
			case '5':
				await this.play('aktivace akustického návěstí na křižovatkách')
				break;
			case '6':
				await this.play('informační tabule příjezdů a odjezdů na nástupištích veřejné dopravy')
				break;
			default:
				break;
		}
	}
}
