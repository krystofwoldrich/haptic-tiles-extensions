import util from 'util'
import say from 'say'
import TalkingSignSystem from './TalkingSignSystem.mjs';
import TalkingSignSystemRemote from './TalkingSignSystemRemote.mjs';
import BrailleModuleFactory from '../BrailleModule/BrailleModuleFactory.mjs';
import KeyboardBrailleModule from '../BrailleModule/KeyboardBrailleModule.mjs';
import MacAudioPlayer from '../Audio/MacAudioPlayer.mjs';
import MuteTextOnlyAudioPlayer from '../Audio/MuteTextOnlyAudioPlayer.mjs';

export default class TalkingSignSystemFactory {

	static async createTSS({ module, mute, language, onlyExamples }) {
		const speakUniversal = util.promisify((...args) => say.speak(...args))
		const speak = (input, voice) => speakUniversal(input, voice, 1)

		let brailleModule;
		if (module === 'keyboard') {
			brailleModule = new KeyboardBrailleModule();
		} else {
			brailleModule = await BrailleModuleFactory.createDoubleBrailleModule();
		}
		await brailleModule.init();

		let player;
		if (mute) {
			player = new MuteTextOnlyAudioPlayer(speak);
		} else {
			player = new MacAudioPlayer(speak, language);
		}

		const remote = new TalkingSignSystemRemote(brailleModule);
		const tss = new TalkingSignSystem(
			remote,
			(...args) => player.play(...args),
			onlyExamples,
		);

		await tss.start()
		return tss;
	}

}
