
/**
 * Debounce func until previous call is finished.
 */
export default function debounce(func) {
	let running = false;
	return async (...args) => {
		if (running) {
			return;
		}
		running = true;

		try {
			return await func(...args)
		} finally {
			running = false;
		}
	};
}

