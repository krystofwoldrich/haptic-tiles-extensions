import fs from 'fs-extra';
import debounce from '../Utils/debounce.mjs';
import Debug from 'debug';
import _ from 'lodash';
import { loadData } from './dataLoader.mjs';

const debug = Debug('HapticTilesExtensions:MemoryAssignment');

export default class MemoryAssignment {

	gameId = null;
	keyDownHistory = [];
	matched = new Map();
	dataMap = new Map();
	couplesMap = new Map();
	moves = [];
	matchedCount = 0;
	winCount = null;
	resolveEnd = ()	=> { /* do nothing */ };

	constructor(module, play, dataPath) {
		this.dataPath = dataPath;
		this.module = module;
		this.play = play;
		this._calcWinCount();
	}

	async start() {
		await this._loadData();
		await this._initMoves();
		this.module.place([
			...this.dataMap.keys(),
		]);
		this.module.on(
			'keydown',
			debounce(this._onModuleKeyDown.bind(this)),
		);
		debug(
			'MemoryAssignment started',
			'data',
			this.couplesMap,
			'moves',
			this.moves,
			'board',
			this.board,
		);
		this._saveGameLog();
		await this._playInitialSequence();
	}

	async stop() {
		this.module.on('keydown', () => { /* do nothing */ });
	}

	async waitForEnd() {
		if (this.matchedCount >= this.module.pointsCount) {
			return new Promise.resolve();
		}

		return new Promise(resolve => {
			this.resolveEnd = resolve;
		});
	};

	async _playInitialSequence() {
		const [exampleKey1, exampleKey2] = this.couplesMap.entries().next().value;
		const example1 = this.dataMap.get(exampleKey1);
		const example2 = this.dataMap.get(exampleKey2);

		await this.play('Najděte všechny dvojice, které patří k sobě.');
		await this.play('Například.');
		await this.play(example1);
		await this.play('k tomu patří');
		await this.play(example2);
		await this.play('Můžete začít.');
	}

	_calcWinCount() {
		const maxPoints = this.module.pointsCount;
		this.winCount = Math.floor(maxPoints / 2);
	}

	async _loadData() {
		const [couplesMap, dataMap] = await loadData(this.dataPath);
		this.couplesMap = couplesMap;
		this.dataMap = dataMap;
	}

	async _onModuleKeyDown(currentKey) {
		const timestampKeyDown = (new Date()).toISOString();
		this.keyDownHistory.push({
			key: currentKey,
			timestamp: timestampKeyDown,
		});
		const { dataMap } = this;
		debug('Chosen board coordinates', currentKey);
		const currentOppositeKey = this.couplesMap.get(currentKey);
		const isSecondTurn = this.moves[this.moves.length-1]?.length === 1;
		const currentMove = isSecondTurn
			? this.moves[this.moves.length-1]
			: [];
		const prevTurn = currentMove[currentMove.length-1] ?? null;
		const isAlreadyMatchedTurn = this.matched.get(currentKey);

		if (isAlreadyMatchedTurn) {
			await this.play(dataMap.get(currentKey));
			await this.play(`K tomu jste už našli`);
			await this.play(dataMap.get(currentOppositeKey));
			await this.play(`Zkuste jiné pole.`);
			return;
		}

		if (_.isEqual(currentKey, prevTurn)) {
			await this.play(dataMap.get(currentKey));
			await this.play(`Už jste vybrali. Teď jen najít správnou dvojici.`);
			return;
		}

		await this.play(dataMap.get(currentKey));
		currentMove.push(currentKey);
		if (isSecondTurn) {
			await this._markMatched(currentMove);
		} else { //isFirstTurn (isGameStart)
			this.moves.push(currentMove);
		}

		await this._saveGameLog();
		await this._processWin();
	}

	async _processWin() {
		if (this.matchedCount === (Math.floor(this.module.size/2))) {
			await this.play('Výborně! Vyhrál jste!');
			this.stop();
			this.resolveEnd();
		}
	}

	async _markMatched(move) {
		const isMatchingMove = await this._isMatchingMove(move);
		if (isMatchingMove) {
			this.matched.set(move[0], true);
			this.matched.set(move[1], true);
			this.matchedCount++;
		}
	}

	async _isMatchingMove(move) {
		const firstTurn = move[0];
		const secondTurn = move[1];
		const isMatchingMove = this.couplesMap.get(firstTurn) === secondTurn;

		if (isMatchingMove) {
			await this.play(`Výborně!`);
			await this.play(this.dataMap.get(firstTurn));
			await this.play(`patří dohromady s`);
			await this.play(this.dataMap.get(secondTurn));
		} else {
			await this.play(`Bohužel!`);
			await this.play(this.dataMap.get(firstTurn));
			await this.play(`nepatří dohromady s`);
			await this.play(this.dataMap.get(secondTurn));
			await this.play(`zkuste to znovu.`);
		}

		return isMatchingMove
	}

	_initMoves() {
		this.moves = [];
	}

	async _saveGameLog() {
		if (!this.gameId) {
			this.gameId = (new Date()).toISOString();
		}

		await fs.writeJSON(
			`./logs/${this.gameId}.memory_game.json`,
			{
				keyDownHistory: this.keyDownHistory,
				moves: this.moves,
			},
			{
				spaces: 2,
			},
		);
	}
}
