import MemoryAssignment from './MemoryAssignment.mjs';
import MemoryBoard from './MemoryBoard.mjs';
import BrailleModuleFactory from '../BrailleModule/BrailleModuleFactory.mjs';
import { AudioPlayerFactory } from '../Audio/AudioPlayerFactory.mjs';

export default class GameFactory {

	static async startMemorizeGame({ module, mute, dataPath, language }) {
		const player = AudioPlayerFactory.createAudioPlayer(mute, language);

		const brailleModule = await BrailleModuleFactory.createBrailleModule(module);

		const board = new MemoryBoard(brailleModule);
		const game = new MemoryAssignment(
			board,
			(...args) => player.play(...args),
			dataPath,
		);

		await game.start();
		await game.waitForEnd();
		process.exit();
	}

}
