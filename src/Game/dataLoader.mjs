import path from 'path';
import fs from 'fs-extra';
import _ from 'lodash';

export async function loadData(filePath) {
	const data = await fs.readJson(filePath);
	const isStringToString = _.isString(data[0]?.[0]);
	if (isStringToString) {
		return loadDataStringToString(data, filePath);
	} else {
		return loadDataObjectToObject(data, filePath);
	}
}

export async function loadDataStringToString(data) {
	const couplesMap = new Map();
	for (const couple of data) {
		couplesMap.set(couple[0], couple[1]);
		couplesMap.set(couple[1], couple[0]);
	}
	return [couplesMap, couplesMap];
}

export async function loadDataObjectToObject(data, filePath) {
	const pathPrefix = path.dirname(filePath);
	const couplesMap = new Map();
	const dataMap = new Map();
	for (const couple of data) {
		couple[0].key = couple[0].key ?? couple[0].value;
		couple[1].key = couple[1].key ?? couple[1].value;
		if (couple[0].type === 'sound') {
			couple[0].value = path.join(pathPrefix, couple[0].value);
		}
		if(couple[1].type === 'sound') {
			couple[1].value = path.join(pathPrefix, couple[1].value);
		}
		dataMap.set(couple[0].key, couple[0]);
		dataMap.set(couple[1].key, couple[1]);
		couplesMap.set(couple[0].key, couple[1].key);
		couplesMap.set(couple[1].key, couple[0].key);
	}
	return [couplesMap, dataMap];
}
