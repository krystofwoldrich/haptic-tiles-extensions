import fs from 'fs-extra';
import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:MemoryBoard');

export default class MemoryBoard {

	constructor(
		module,
	) {
		this.module = module;
		this.board = [];
		this.module.on('keydown', (...args) => this._onModuleKeydown(...args));
		this.size = module.size;
	}

	on(event, callback) {
		if (event === 'keydown') {
			this.keydownCallback = callback;
		}
	}

	place(data) {
		const max = this.module.size;
		this.board = data.slice(0, max);
		this.board = this.board
			.map(value => ({ value, sort: Math.random() }))
			.sort((a, b) => a.sort - b.sort)
			.map(({ value }) => value);

		debug('place', this.board);
		fs.writeJSONSync(
			`./logs/${(new Date()).toISOString()}.board.json`,
			this.board,
			{
				spaces: 2,
			},
		);

		if (data.length > max) {
			console.warn('MemoryBoard: too many data', data.length, max);
		}
	}

	_onModuleKeydown(brailleSymbol) {
		debug('pressed braille', brailleSymbol);
		if (!this.keydownCallback) {
			return;
		}

		const key = this._brailleToKey(brailleSymbol);
		this.keydownCallback(key);
	}

	_brailleToKey({ symbol, braille }) {
		const { board } = this;
		const index = (symbol * 6) + (braille - 1);
		debug('braille to key', symbol, braille, index);
		return board[index];
	}
}
