import Debug from 'debug';

const debug = Debug('HapticTilesExtensions:DoubleBrailleModule');

const buttonToBraille = {
	  '10': '1',
	  '20': '2',
	  '30': '3',

	  '11': '4',
	  '21': '5',
	  '31': '6',

	  '13': '1',
	  '23': '2',
	  '33': '3',

	  '14': '4',
	  '24': '5',
	  '34': '6',
}

const leftSymbol = {
	'10': true,
	'20': true,
	'30': true,

	'11': true,
	'21': true,
	'31': true,
}

const isLeftSymbol = (key) => leftSymbol[key] === true;

export default class DoubleBrailleModule {

	size = 12;

	constructor(
		tiles,
	) {
		this.tiles = tiles;
	}

	async init() {
		await this.tiles.init();
		this.tiles.on('keydown', (...args) => this._onTileKeydown(...args));
	}

	on(event, callback) {
		if (event === 'keydown') {
			this.keydownCallback = callback;
		}
	}

	_onTileKeydown(data) {
		debug('_onTileKeydown', data);
		if (this.keydownCallback) {
			const brailleKey = `${data.button.row}${data.button.column}`;

			this.keydownCallback({
				symbol: isLeftSymbol(brailleKey) ? data.tile : data.tile + 1,
				braille: buttonToBraille[brailleKey],
			});
		}
	}

}
