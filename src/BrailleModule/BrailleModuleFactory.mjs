import HapticTilesFactory from '../HapticTiles/HapticTilesFactory.mjs';
import DoubleBrailleModule from './DoubleBrailleModule.mjs';
import KeyboardBrailleModule from './KeyboardBrailleModule.mjs';

export default class BrailleModuleFactory {

	static async createDoubleBrailleModule() {
		const tiles = await HapticTilesFactory.createHapticTilesClient();
		const module = new DoubleBrailleModule(tiles);
		await module.init();

		return module;
	}

	static async createKeyboardBrailleModule() {
		const module = new KeyboardBrailleModule();
		await module.init();

		return module;
	}

	static async createBrailleModule(moduleClass) {
		let module;
		if (moduleClass === 'keyboard') {
			module = new KeyboardBrailleModule();
		} else {
			module = await BrailleModuleFactory.createDoubleBrailleModule();
		}
		await module.init();

		return module;
	}

}
