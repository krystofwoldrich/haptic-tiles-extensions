import util from 'util'
import say from 'say'
import MuteTextOnlyAudioPlayer from './MuteTextOnlyAudioPlayer.mjs'
import MacAudioPlayer from './MacAudioPlayer.mjs'

export class AudioPlayerFactory {

	static createAudioPlayer(mute, language) {
		const speakUniversal = util.promisify((...args) => say.speak(...args))
		const speak = (input, voice) => speakUniversal(input, voice, 1)

		let player;
		if (mute) {
			player = new MuteTextOnlyAudioPlayer(speak);
		} else {
			player = new MacAudioPlayer(speak, language);
		}

		return player;
	}

}
