#!/usr/bin/env node
import { program } from 'commander';
import EducationFactory from './Education/EducationFactory.mjs';
import GameFactory from './Game/GameFactory.mjs';
import TalkingSignSystemFactory from './TalkingSignSystem/TalkingSignSystemFactory.mjs';

const MODULE_OPTION = ['-m, --module <module>', 'Connected Braille module, keyboard, doubleBraille', 'doubleBraille'];
const MUTE_OPTION = ['--mute', 'Mute audio output', 0];
const LANGUAGE_OPTION = ['-l, --language <language>', 'Language cs-cz, en-us', 'cs-cz'];

program
	.name('haptic-tiles-extensions')
	.description('HapticTiles extensions cli testing tool')
	.version('0.0.1');

program.command('tss')
	.description('Run TalkingSignSystem Numbering Education')
	.option('--only-examples', 'Only run examples', 0)
	.option(...MODULE_OPTION)
	.option(...MUTE_OPTION)
	.option(...LANGUAGE_OPTION)
	.action(TalkingSignSystemFactory.createTSS);

program.command('char-education')
	.description('Run Character Education')
	.requiredOption('-d, --data-path <path>', 'Path to braille characters data set')
	.option(...MODULE_OPTION)
	.option(...MUTE_OPTION)
	.option(...LANGUAGE_OPTION)
	.action(EducationFactory.createCharacterLearning);

program.command('memorize-game')
	.description('Run Memorize-Assigning Game')
	.requiredOption('-d, --data-path <path>', 'Path to braille characters data set')
	.option(...MODULE_OPTION)
	.option(...MUTE_OPTION)
	.option(...LANGUAGE_OPTION)
	.action(GameFactory.startMemorizeGame);

program.command('braille-numbering')
	.description('Run Braille Numbering to get user familiar with order of braille dots')
	.option(...MODULE_OPTION)
	.option(...MUTE_OPTION)
	.option(...LANGUAGE_OPTION)
	.action(EducationFactory.startBrailleNumbering);

program.parse();
