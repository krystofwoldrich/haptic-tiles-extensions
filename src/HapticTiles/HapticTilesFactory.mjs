import delay from 'delay';
import { SerialPort } from 'serialport';
import Debug from 'debug';
import HapticTilesClient from './HapticTilesClient.mjs';

const debug = Debug('HapticTilesExtensions:HapticTilesFactory');

const HAPTIC_TILES_SERIAL_PORT = process.env.HAPTIC_TILES_SERIAL_PORT;
const HAPTIC_TILES_SERIAL_PORT_BAUD_RATE = parseInt(process.env.HAPTIC_TILES_SERIAL_PORT_BAUD_RATE);

export default class HapticTilesFactory {

	static async createHapticTilesClient() {
		if (!HAPTIC_TILES_SERIAL_PORT) {
			throw new Error('HAPTIC_TILES_SERIAL_PORT variable is not set');
		}
		if (!HAPTIC_TILES_SERIAL_PORT_BAUD_RATE) {
			throw new Error('HAPTIC_TILES_SERIAL_PORT_BAUD_RATE variable is not set');
		}

		const serialPort = new SerialPort(
			{
				path: HAPTIC_TILES_SERIAL_PORT,
				baudRate: HAPTIC_TILES_SERIAL_PORT_BAUD_RATE,
				endOnClose: false,
				autoOpen: false,
			},
			(err) => {
				if (err) {
					debug('Error opening serial port:', err);
					throw err;
				} else {
					debug(
						'Opened serial port',
						serialPort.port,
						serialPort.baudRate,
					);
				}
			},
		);

		serialPort.on('close', async () => {
			debug('Serial port closed');
			await waitForSuccessfulOpen(serialPort);
		});

		await waitForSuccessfulOpen(serialPort);
		const tiles = new HapticTilesClient(serialPort);
		return tiles;
	}

}

async function waitForSuccessfulOpen(serialPort) {
	debug('Waiting for serial port to open...');
	while(!serialPort.isOpen) {
		try {
			await open(serialPort);
			await delay(100);
		} catch {
		}
	}
	debug('Serial port is open');
}

function open(serialPort) {
	return new Promise((resolve, reject) => {
		serialPort.open((err) => {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
}
