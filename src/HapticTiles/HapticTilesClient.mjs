import Debug from 'debug';
import delay from 'delay';
import { ReadlineParser } from 'serialport';

const debug = Debug('HapticTilesExtensions:HapticTilesClient')

const SELF_INIT_TIMEOUT = 5000;

const NOT_INITIALIZED = 'NOT_INITIALIZED';
const INITIALIZING = 'INITIALIZING';
const INITIALIZED = 'INITIALIZED';

const ALIVE_EVENT = 'alive';
const TOPOLOGY_EVENT = 'topo';
const BUTTON_EVENT = 'button';

export default class HapticTilesClient {

	static INIT_MESSAGE = {
		event: 'init',
		order: 0,
	};

	static DEBUG_OFF_MESSAGE = {
		event: "pixel",
		order: 0,
		i: 0,
		j: 0,
		r: 0,
		g: 0,
		b:0,
	};

	state = NOT_INITIALIZED;
	topology = [];

	constructor(serialPort) {
		this.serialPort = serialPort;
		this.serialPort.on('close', () => {
			debug('Tiles client state restarted');
			this.state = NOT_INITIALIZED;
		});

		this.parser = serialPort.pipe(new ReadlineParser());
		this.parser.on('data', this._onData.bind(this));
		this.selfInitTimeout = setTimeout(() => {
			debug('Tiles client self init timeout');
			this.init();
		}, SELF_INIT_TIMEOUT);
	}

	on(event, callback) {
		if (event === 'keydown') {
			this.keydownCallback = callback;
		}
	}

	async init() {
		clearTimeout(this.selfInitTimeout);
		if (this.state === NOT_INITIALIZED) {
			this.state = INITIALIZING;
			await this._write(HapticTilesClient.INIT_MESSAGE);
		}
	}

	async debugOff() {
		for(const key of this.topology.keys()) {
			await this._write({
				...HapticTilesClient.DEBUG_OFF_MESSAGE,
				order: key,
			});
			await delay(50);
		}
	}

	async _onData(data) {
		const messageRaw = data.toString();
		debug('Received data:', messageRaw);

		const message = JSON.parse(messageRaw);

		switch (message.event) {
			case ALIVE_EVENT:
				await this.init();
				break;

			case TOPOLOGY_EVENT:
				this.topology = message.data;
				this.state = INITIALIZED;
				await this.debugOff();
				break;

			case BUTTON_EVENT:
				if (this.keydownCallback) {
					this.keydownCallback({
						tile: message.order,
						button: {
							row: message.i,
							column: message.j,
						},
					});
				}
				break;

			default:
				debug('Unknown event:', message.event);
				break;
		}
	}

	_write(message) {
		debug('Sending data:', message);
		return new Promise((resolve, reject) => {
			this.serialPort.write(JSON.stringify(message), (err) => {
				if (err) {
					reject(err);
				} else {
					resolve();
				}
			});
		});
	}

}
